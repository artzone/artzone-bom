# artzone-bom

## Maven
### Checking for new plugin updates
```
./mvnw --settings .m2/settings.xml versions:display-plugin-updates
```
### Checking  for updated dependencies in repository
```
./mvnw --settings .m2/settings.xml versions:display-dependency-updates
```
  